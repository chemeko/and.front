export const environment = {
  url_api: "https://api-interno.www.gov.co/api/",
  serviceUrls: {
    home: {
      bannerPrincipal: "home/BannerPrincipal",
      bannerInformativo: "home/BannerInformativo",
    },
    noticias: {
      seccionActualidadGeneral: "noticias/secciones/actualidad-general",
      allCategorias: "noticias/Noticias/categorias/0",
    },
  },
};