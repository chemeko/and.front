import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from "@angular/common/http";
import { throwError, Observable, of, concat } from "rxjs";
import { catchError, retryWhen, concatMap, delay, take } from "rxjs/operators";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class GeneralService {
  API_URL = environment.url_api;

  constructor(private http: HttpClient) {}

  public get<T>(url: string, params?: HttpParams): Observable<any> {
    const headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http
      .get<T>(`${this.API_URL}${url}`, { headers, params }) // se comenta el '/' de apir url y url
      .pipe(
        retryWhen(
          (
            errors // Tolerancia de errores
          ) =>
            errors.pipe(
              concatMap((result) => {
                if (result.status === 504) {
                  return of(result);
                }
                return throwError(result);
              }),
              delay(1000),
              take(4),
              (o) =>
                concat(
                  o,
                  throwError(`No fue posible conectarse con el servidor.`)
                )
            )
        ),
        catchError((err: HttpErrorResponse) => {
          return this.handleError(err);
        })
      );
  }

  public post<T>(
    url: string,
    model: any,
    params?: HttpParams
  ): Observable<any> {
    const headers = new HttpHeaders().set("Content-Type", "application/json");

    return this.http
      .post<T>(`${this.API_URL}${url}`, model, { headers, params })
      .pipe(
        retryWhen(
          (
            errors // Tolerancia de errores
          ) =>
            errors.pipe(
              concatMap((result) => {
                if (result.status === 504) {
                  return of(result);
                }
                return throwError(result);
              }),
              delay(1000),
              take(4),
              (o) =>
                concat(
                  o,
                  throwError(`No fue posible conectarse con el servidor.`)
                )
            )
        ),
        catchError((err: HttpErrorResponse) => {
          return this.handleError(err);
        })
      );
  }

  public delete<T>(
    url: string,
    model: any,
    params?: HttpParams
  ): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      body: model,
    };

    return this.http.delete<T>(`${this.API_URL}${url}`, httpOptions).pipe(
      retryWhen(
        (
          errors // Tolerancia de errores
        ) =>
          errors.pipe(
            concatMap((result) => {
              if (result.status === 504) {
                return of(result);
              }
              return throwError(result);
            }),
            delay(1000),
            take(4),
            (o) =>
              concat(
                o,
                throwError(`No fue posible conectarse con el servidor.`)
              )
          )
      ),
      catchError((err: HttpErrorResponse) => {
        return this.handleError(err);
      })
    );
  }

  handleError(error: HttpErrorResponse) {
    let messageError: string;
    if (error.status == 404) {
      messageError = `<p>${error.error.message}</p>`;
      console.log("error: " + messageError);
    } else if (error.status == 403 || error.status == 401) {
      messageError =
        "Indicando el siguiente error: </p><p><b>" +
        "No autorizado" +
        "</b></p>";
      // log de errores
      console.log("error: " + messageError);
    } else {
      messageError =
        "Indicando el siguiente error: </p><p><b>" + error.message + "</b></p>";
      // log de errores
      console.log("error: " + messageError);
    }
    return throwError(error);
  }
}
