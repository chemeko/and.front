import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { DatosBannerPrincipal } from "../../models/datosBannerPrincipal";
import { BannerPrincipalService } from "../../services/bannerPrincipal.service";

@Component({
  selector: "andPrueba-bannerPrincipal",
  templateUrl: "./bannerPrincipal.component.html",
  styleUrls: ["./bannerPrincipal.component.scss"],
})
export class BannerPrincipalComponent implements OnInit, OnDestroy {
  public seccionBannerPrincipal: DatosBannerPrincipal =
    {} as DatosBannerPrincipal;
  public urlImagen: string = "";
  public textoDescriptivo: string = "";
  pageSuscription = new Subscription();

  constructor(private bannerPrincipalService: BannerPrincipalService) {}

  ngOnInit() {
    this.cargarBannerPrincipal();
  }

  cargarBannerPrincipal() {
    this.pageSuscription.add(
      this.bannerPrincipalService.getSeccionBannerPrincipal().subscribe(
        (result) => {
          this.seccionBannerPrincipal = result.data;
          this.urlImagen =
            this.seccionBannerPrincipal.listaImagenes[0].urlImagen;
          this.textoDescriptivo =
            this.seccionBannerPrincipal.listaImagenes[0].textoDescriptivo;
          console.log(this.seccionBannerPrincipal);
        },
        (error) => {
          //pendiente agregar algo adicional
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.pageSuscription.unsubscribe();
  }
}
