import { Component, OnInit } from "@angular/core";

@Component({
  selector: "andPrueba-accessibility",
  templateUrl: "./accessibility.component.html",
  styleUrls: ["./accessibility.component.scss"],
})
export class AccessibilityComponent implements OnInit {
  contrasteActivo: boolean = false;
  classes = ["f0", "f1", "f2", "f3", "f4"];
  classIndex = 2;

  constructor() {}

  ngOnInit(): void {
    this.contrasteActivo = false;
  }

  reducirFuente(){
    var previousClass = this.classIndex;
    this.classIndex--;
    this.classIndex = (this.classIndex < 0) ? 0 : this.classIndex;
    this.cambiarClase(previousClass, this.classIndex);
  }

  aumentarFuente(){
    var previousClass = this.classIndex;
    this.classIndex++;
    this.classIndex = (this.classIndex == this.classes.length) ? this.classes.length - 1 : this.classIndex;
    this.cambiarClase(previousClass, this.classIndex);
  }

  cambiarClase(previous: any, next: any) {
    if (previous != next) {
      var htmlElement = document.querySelector("html");
      if (htmlElement != null) {
        htmlElement.classList.remove(this.classes[previous]);
        htmlElement.classList.add(this.classes[next]);
      }
    }
  }

  cambiarContraste(): void {
    var body = document.body;
    if (body) {
      if (localStorage.getItem("contraste") == "S") {
        body.classList.remove("altoContraste");
        localStorage.setItem("contraste", "N");
      } else {
        body.classList.add("altoContraste");
        localStorage.setItem("contraste", "S");
      }
    }
  }
}
