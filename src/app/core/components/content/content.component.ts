import { Component, OnInit } from "@angular/core";

@Component({
  selector: "andPrueba-content",
  templateUrl: "./content.component.html",
  styleUrls: ["./content.component.scss"],
})
export class ContentComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  onActivate(e: any) {
    window.scroll(0, 0);
  }
}