import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GeneralService } from 'src/app/shared/services/general/general.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BannerPrincipalService {

  constructor(private general: GeneralService) { }

  getSeccionBannerPrincipal() :Observable <any>{
    return this.general.get(environment.serviceUrls.home.bannerPrincipal);
  }
}
