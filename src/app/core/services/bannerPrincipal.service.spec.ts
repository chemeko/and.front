/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BannerPrincipalService } from './bannerPrincipal.service';

describe('Service: BannerPrincipal', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BannerPrincipalService]
    });
  });

  it('should ...', inject([BannerPrincipalService], (service: BannerPrincipalService) => {
    expect(service).toBeTruthy();
  }));
});
