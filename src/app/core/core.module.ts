import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateComponent } from './components/template/template.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AccessibilityComponent } from './components/accessibility/accessibility.component';
import { BannerPrincipalComponent } from './components/bannerPrincipal/bannerPrincipal.component';
import { ContentComponent } from './components/content/content.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    TemplateComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    AccessibilityComponent,
    BannerPrincipalComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    TemplateComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [HttpClientModule]
})
export class CoreModule { }