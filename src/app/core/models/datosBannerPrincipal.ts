export interface DatosBannerPrincipal {
  textoBienvenida: string;
  textoBuscador: string;
  textoAuxiliar: string;
  textoBotonAuxiliar: string;
  urlBotonAuxiliar: string;
  listaImagenes: imagen [];
}

export interface imagen {
  urlImagen: string;
  textoDescriptivo: string;
}
