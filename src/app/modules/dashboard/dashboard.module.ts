import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DashboardComponent } from "./dashboard.component";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { ContactosEmergenciaComponent } from "./components/contactosEmergencia/contactosEmergencia.component";
import { ActualidadComponent } from "./components/actualidad/actualidad.component";

@NgModule({
  declarations: [DashboardComponent, ContactosEmergenciaComponent, ActualidadComponent],
  imports: [CommonModule, DashboardRoutingModule],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class DashboardModule {}
