/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActualidadService } from './actualidad.service';

describe('Service: Actualidad', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActualidadService]
    });
  });

  it('should ...', inject([ActualidadService], (service: ActualidadService) => {
    expect(service).toBeTruthy();
  }));
});
