import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GeneralService } from 'src/app/shared/services/general/general.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ActualidadService {

  constructor(private general: GeneralService) { }

  getSeccionActualidadGeneral() :Observable <any> {
    return this.general.get(environment.serviceUrls.noticias.seccionActualidadGeneral);
  }

  getAllCategorias() :Observable <any>  {
    return this.general.get(environment.serviceUrls.noticias.allCategorias);
  }
}
