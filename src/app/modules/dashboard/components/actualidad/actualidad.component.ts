import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { Categoria } from "../../models/categoria";
import { SeccionActualidad } from "../../models/seccionActualidad";
import { ActualidadService } from "../../services/actualidad.service";

@Component({
  selector: "andPrueba-actualidad",
  templateUrl: "./actualidad.component.html",
  styleUrls: ["./actualidad.component.scss"],
})
export class ActualidadComponent implements OnInit, OnDestroy {
  public listCategoria: Categoria[] = [];
  public seccionActualidad: SeccionActualidad = {} as SeccionActualidad;
  pageSuscription = new Subscription();
  constructor(private actualidadService: ActualidadService) {}

  ngOnInit() {
    this.cargarSeccionActualidad();
    this.cargarCategorias();
  }

  cargarSeccionActualidad() {
    this.pageSuscription.add(
      this.actualidadService.getSeccionActualidadGeneral().subscribe(
        (result) => {
          this.seccionActualidad = result.data;
          console.log(this.seccionActualidad);
        },
        (error) => {
          //pendiente agregar algo adicional
        }
      )
    );
  }

  cargarCategorias() {
    this.pageSuscription.add(
      this.actualidadService.getAllCategorias().subscribe(
        (result) => {
          this.listCategoria = result.data;
          console.log(this.listCategoria);
        },
        (error) => {
          //pendiente agregar algo adicional
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.pageSuscription.unsubscribe();
  }
}
