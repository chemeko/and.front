export interface Categoria {
    id: number;
    titulo: string;
    sumario: string;
    imagen: string;
    textoAlternativo: string;
    textoFecha: string;
}
