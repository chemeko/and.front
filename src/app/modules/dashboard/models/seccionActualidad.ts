export interface SeccionActualidad {
  id: number;
  titulo: string;
  descripcion: string;
  botonTexto: string;
  botonTextoAlternativo: string;
}
